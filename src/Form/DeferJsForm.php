<?php

/**
 * @file
 * Contains \Drupal\deferjs\Form\DeferJsForm.
 */

namespace Drupal\deferjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class DeferJsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'deferjs_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('deferjs.settings');
    $form['settings']['module_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable deferjs'),
      '#default_value' => $config->get('module_enable'),
      '#description' => $this->t('This will enable deferjs library on website.')
    ];
    $types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $content_types[''] = $this->t("Select All");
    foreach($types as $type) {
      $content_types[$type->id()] = $type->label();
    }
    $form['settings']['enabled_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Content Types'),
      '#default_value' => $config->get('enabled_content_types'),
      '#description' => $this->t('by default this will enabled for all content type, un-check if you want exclude any content type'),
      '#options' => $content_types,
    ];
    $form['settings']['exclude_page'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude Pages'),
      '#default_value' => $config->get('exclude_page'),
      '#description' => $this->t('Path of pages to be exclude from defer. For multiple pages use line break.')
    ];
    $form['settings']['exclude_file'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude JS files'),
      '#default_value' => $config->get('exclude_file'),
      '#description' => $this->t('Path of files to be exclude from defer. For multiple files use line break.')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('deferjs.settings');
    $config->set('module_enable', $form_state->getValue('module_enable'));
    $config->set('enabled_content_types', $form_state->getValue('enabled_content_types'));
    $config->set('exclude_file', $form_state->getValue('exclude_file'));
    $config->set('exclude_page', $form_state->getValue('exclude_page'));
    $config->save();
    drupal_flush_all_caches();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}.
   */
  protected function getEditableConfigNames() {
    return [
      'deferjs.settings',
    ];
  }
}

