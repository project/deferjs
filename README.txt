deferjs module is based on a tiny library https://github.com/shinsenter/defer.js/
Using this library we can lazy load static resources like js and css.
This library also provide functionality of defering js files.
In this module we have funtionality to defer js.
For other functionailties like custom delay css and js, lazyloading etc please refer to available option from library page https://github.com/shinsenter/defer.js

Configration
after installation, go to admin/config/performance/deferjs
keep the setting as per you use.